var Promise = require('bluebird');
var adb = require('adbkit');
var client = adb.createClient();
var fs = require('fs');
var uniqid = require('uniqid');

const apk = 'vendor/app.apk';
const reportFileName = './reportFile.html';
const studentCode = 201629945;
const lastDigitStudentCode = 5;
const backXNumber = (studentCode % 4) + lastDigitStudentCode;

const arg1 = process.argv[0];
const arg2 = process.argv[1];
const arg3 = parseInt(process.argv[2]);
const arg4 = parseInt(process.argv[3]) ? parseInt(process.argv[3]) : 3000;

const event_codes = {
    'KEYCODE_HOME': 3,
    'KEYCODE_BACK': 4,
    'KEYCODE_APP_SWITCH': 187
}

if (!arg3) {
    console.error('To initialize the app, you need to run: node index.js {integer_number}');
    return;
}

var numberOfEventsPerformed = 0;


function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

const performNewShell = async function (deviceId, command) {
    let response = await client.shell(deviceId, command).then(adb.util.readAll);
    return response;
}

const performNewShellWithResult = async function (deviceId, command, resolveMessage, rejectMessage) {
    let result = 'An error ocurred. ' + rejectMessage;
    let response = await performNewShell(deviceId, command);
    if (!response.toString().toLowerCase().includes('Error')) {
        if (!resolveMessage) {
            resolveMessage = response.toString();
        }
        result = `<p>[${new Date()}]: ${resolveMessage}</p>`;
    }
    let id = uniqid();
    await sleep(arg4);
    response = (await performNewShell(deviceId, `screencap /sdcard/${id}.png`));
    let transfer = await client.pull(deviceId, `/sdcard/${id}.png`);
    await performNewShell(deviceId, `rm -r /sdcard/${id}.png`);
    response = await new Promise(function (resolve, reject) {
        transfer.on('end', function () {
            resolve();
        });
        transfer.on('error', reject)
        transfer.pipe(fs.createWriteStream(`${__dirname}/data/${id}.png`));
    });
    await new Promise(function (resolve, reject) {
        fs.readFile(`${__dirname}/data/${id}.png`, function (err, data) {
            if (err) {
                result += `<img src='' alt='error recovering image'/>`;
                reject();
            }
            result += `<img src='data:image/png;base64, ${data.toString('base64')}' width='100px'/>`;
            resolve();
        })
    })
    return result;
}

const question1Event1 = async function (deviceId) {
    let body = '';
    await performNewShell(deviceId, `input keyevent ${event_codes.KEYCODE_HOME}`);
    await sleep(1000);
    body += (await performNewShellWithResult(deviceId, `input tap 250 2220`,
        'First app on launcher opened', 'Could not open the first app on launcher')).toString();

    return body;
}


const question1Event2 = async function (deviceId) {
    let body = '';
    let packageName = (await performNewShell(deviceId, `dumpsys activity | grep top-activity`)).toString();
    packageName = /(?<=trm: [0-9] [0-9]{0,}:)(.*)(?=\/)/.exec(packageName);
    if (packageName) {
        packageName = packageName[0];
        body += await performNewShellWithResult(deviceId, `am force-stop ${packageName}`,
            'Current app closed.', 'Could not close the current app.');
    }
    else {
        body += '<p>Could not retrieve the current process to exit the app.</p>';
    }
    return body;
}

const question1Event3 = async function (deviceId) {
    let body = `<p>[${new Date()}]: The device's display is:</p>`;
    body += await performNewShellWithResult(deviceId,
        `dumpsys window | grep cur= |tr -s " " | cut -d " " -f 4|cut -d "=" -f 2`,
        null, 'Could not retrieve devices display');
    return body;
}

const question1Event4 = async function (deviceId) {
    let body = '';
    body += await performNewShellWithResult(deviceId,
        `am start -a android.settings.BLUETOOTH_SETTINGS`,
        'Bluetooth settings opened', 'Could not open bluetooth settings');
    let bluetooth_on = parseInt(await performNewShell(deviceId, 'settings get global bluetooth_on'));
    if (bluetooth_on) {
        body += `<p>[${new Date()}]:Bluetooth already on</p>`;
    }
    else {
        body += await performNewShell(deviceId,
            `input keyevent 23`);
        body += await performNewShellWithResult(deviceId,
            `input keyevent 23`,
            'Bluetooth enabled', 'Could not enable bluetooth');
        let packageName = (await performNewShell(deviceId, `dumpsys activity | grep top-activity`)).toString();
        packageName = /(?<=trm: [0-9] [0-9]{0,}:)(.*)(?=\/)/.exec(packageName);
        if (packageName) {
            packageName = packageName[0];
            body += await performNewShell(deviceId, `am force-stop ${packageName}`);
        }
    }
    return body;
}

const question1Event5 = async function (deviceId) {
    let body = '';
    body += await performNewShellWithResult(deviceId,
        `am start -a android.intent.action.INSERT -t vnd.android.cursor.dir/contact -e name \'Número de eventos ${arg3}\' -e phone 123456789`,
        'new contact added', 'Could not retrieve devices display');
    let packageName = (await performNewShell(deviceId, `dumpsys activity | grep top-activity`)).toString();
    packageName = /(?<=trm: [0-9] [0-9]{0,}:)(.*)(?=\/)/.exec(packageName);
    if (packageName) {
        packageName = packageName[0];
        body += await performNewShell(deviceId, `am force-stop ${packageName}`);
    }
    // await performNewShell(deviceId, 'input keyevent ' + event_codes['KEYCODE_HOME']);
    return body;
}

const question2Event1 = async function (deviceId) {
    let body = '';

    await performNewShell(deviceId,
        `input keyevent ${event_codes.KEYCODE_HOME}`);
    body += await performNewShellWithResult(deviceId, `input touchscreen swipe 250 2220 250 2220 3000`,
        'First app on launcher long cliked', 'Could not launch the first app');
    await performNewShell(deviceId,
        `input keyevent ${event_codes.KEYCODE_BACK}`);
    body += await performNewShellWithResult(deviceId, `input touchscreen swipe 545 2220 545 2220 3000`,
        'Second app on launcher long clicked', 'Could not click the second app');
    await performNewShell(deviceId,
        `input keyevent ${event_codes.KEYCODE_BACK}`);
    body += await performNewShellWithResult(deviceId, `input touchscreen swipe 855 2220 855 2220 3000`,
        'Third app on launcher long clicked', 'Could not click the third app');
    await performNewShell(deviceId,
        `input keyevent ${event_codes.KEYCODE_BACK}`);

    return body;
}

const question2Event2 = async function (deviceId) {
    let body = '';
    await performNewShell(deviceId, 'service call statusbar 1');
    body += await performNewShellWithResult(deviceId, `dumpsys wifi | grep "Wi-Fi is"`,
        null, 'Could not retrieve the Wi-Fi status');
    await performNewShell(deviceId, 'service call statusbar 2');
    return body;
}

const question2Event3 = async function (deviceId) {
    let body = '';
    // body = await performNewShell(deviceId, 'content insert --uri content://settings/system --bind name:s:accelerometer_rotation --bind value:i:0');
    // console.log('body', body.toString());

    await performNewShell(deviceId, 'service call statusbar 1');
    body += await performNewShellWithResult(deviceId, `content insert --uri content://settings/system --bind name:s:accelerometer_rotation --bind value:i:1`,
        'Rotation lock activated', 'Could not activate the rotations lock');
    await performNewShell(deviceId, 'service call statusbar 2');
    return body;
}


client.listDevices()
    .then(async function (devices) {
        let header = '';
        let body = '';
        let currentDate;
        if (devices.length <= 0) {
            let noDevicesMsg = 'No devices found. Check that the devices is propertly connected. You can confirm by yourself using the command \'adb devices\'';
            console.log(noDevicesMsg);
            body += `<h1>${noDevicesMsg}</h1>`;
        }
        await Promise.map(devices, async function (device) {
            let installed = await client.install(device.id, apk);

            currentDate = new Date();
            body += `<p>[${currentDate}]: App installed</p>`;
            body += await performNewShellWithResult(device.id,
                'am start -n com.example.hello_world/com.example.hello_world.MainActivity',
                'Installed app launched', 'Could not launch the installed app');
            let everyEventQuestion = [
                question1Event1,
                question1Event2,
                question1Event3,
                question1Event4,
                question1Event5,
                question1Event1,
                question2Event1,
                question2Event2,
                question2Event3,
                question1Event5,
            ]
            while (numberOfEventsPerformed < arg3) {
                try {
                    body += await everyEventQuestion[numberOfEventsPerformed % everyEventQuestion.length](device.id);
                    numberOfEventsPerformed++;
                    console.log('numberOfEventsPerformed', numberOfEventsPerformed);

                    if (numberOfEventsPerformed % backXNumber == 0) {
                        body += await performNewShellWithResult(device.id,
                            `input keyevent ${event_codes.KEYCODE_BACK}`,
                            'Back button pressed', 'Could not pressed the back button');
                    }
                }
                catch (err) {
                    console.error(err);
                }
            }

            let uninstalled = await client.uninstall(device.id, 'com.example.hello_world');
            body += `<p>[${currentDate}]: App uninstalled</p>`;

            return;
        });
        return `<!DOCTYPE html><html><head>${header}</head><body>${body}</body></html>`;
    })
    .then(function (html) {
        let stream = fs.createWriteStream(reportFileName);
        stream.once('open', function (fd) {
            stream.end(html);
        });

    })
    .catch(function (err) {
        console.error('Something went wrong:', err.stack);
    })